function slowScroll(id) {
    var offset = 0;
    $('html, body').animate({
        scrollTop: $(id).offset().top - offset
    }, 1000);
    return false;
}
$(".main .menu").click(function() {
    $('.main .anim_menu ul').slideToggle(300);
});

$(document).mouseup(function (e) {
    var container = $(".main .anim_menu ul");
    if (container.has(e.target).length === 0){
        container.hide();
    }
});
